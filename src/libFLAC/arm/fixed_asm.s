@  libFLAC - Free Lossless Audio Codec library
@  Copyright (C) 2001,2002,2003,2004,2005,2006  Josh Coalson
@
@  Redistribution and use in source and binary forms, with or without
@  modification, are permitted provided that the following conditions
@  are met:
@
@  - Redistributions of source code must retain the above copyright
@  notice, this list of conditions and the following disclaimer.
@
@  - Redistributions in binary form must reproduce the above copyright
@  notice, this list of conditions and the following disclaimer in the
@  documentation and/or other materials provided with the distribution.
@
@  - Neither the name of the Xiph.org Foundation nor the names of its
@  contributors may be used to endorse or promote products derived from
@  this software without specific prior written permission.
@
@  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
@  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
@  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
@  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
@  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
@  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
@  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES@ LOSS OF USE, DATA, OR
@  PROFITS@ OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
@  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
@  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
@  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	.text
	.align	2
	.global	FLAC__fixed_restore_signal_asm_arm
	.type	FLAC__fixed_restore_signal_asm_arm, %function
FLAC__fixed_restore_signal_asm_arm:
	stmfd	r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r14}
	@	r0 = residual;
	@	r1 = data_len;
	@	r2 = order;
	@	r3 = data;

	cmp	r2, #4
	ldrls	r15, [r15, r2, asl #2]
	ldmfd	r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}

	.align	2
	.word	.Lorder0
	.word	.Lorder1
	.word	.Lorder2
	.word	.Lorder3
	.word	.Lorder4

	@ data[i] = residual[i];
.Lorder0:
	tst	r1, #15
	beq	.Lorder0b
.Lorder0a:
	@ start off slow until we get to (data_len % 16) == 0
	ldr	r2, [r0], #4
	str	r2, [r3], #4
	subs	r1, r1, #1
	ldmeqfd	r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}
	tst	r1, #15
	bne	.Lorder0a
.Lorder0b:
	@ WHEEEEEEEEEEEEEE!!!!!!!!!!!!!!!!!!!
	ldmia	r0!, {r5 - r12}
	stmia	r3!, {r5 - r12}
	ldmia	r0!, {r5 - r12}
	stmia	r3!, {r5 - r12}
	subs	r1, r1, #16
	bne	.Lorder0b
	ldmfd	r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}

	@ data[i] = residual[i] + data[i-1];
.Lorder1:
	ldr	r14, [r3,#-4]
	tst	r1, #7
	beq	.Lorder1b
.Lorder1a:
	ldr	r2, [r0], #4
	add	r14, r2, r14
	str	r14, [r3], #4
	subs	r1, r1, #1
	ldmeqfd	r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}
	tst	r1, #7
	bne	.Lorder1a
.Lorder1b:
	ldmia	r0!, {r2, r4 - r10}
	add	r2, r2, r14
	add	r4, r4, r2
	add	r5, r5, r4
	add	r6, r6, r5
	add	r7, r7, r6
	add	r8, r8, r7
	add	r9, r9, r8
	add	r14, r10, r9
	stmia	r3!, {r2, r4 - r9, r14}
	subs	r1, r1, #8
	bne	.Lorder1b
	ldmfd	r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}

	@ data[i] = residual[i] + (data[i-1]<<1) - data[i-2];
.Lorder2:
	@ r12 = data[i-2], r14 = data[i-1]
	ldmdb	r3, {r12, r14}
	tst	r1, #7
	beq	.Lorder2b
.Lorder2a:
	ldr	r2, [r0], #4
	add	r2, r2, r14, asl #1
	sub	r2, r2, r12
	str	r2, [r3], #4
	mov	r12, r14
	mov	r14, r2
	subs	r1, r1, #1
	ldmeqfd	r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}
	tst	r1, #7
	bne	.Lorder2a
.Lorder2b:
	ldmia	r0!, {r2, r4 - r10}

	@ r12 = data[i-2], r14 = data[i-1]
	add	r2, r2, r14, asl #1
	sub	r2, r2, r12

	add	r4, r4, r2, asl #1
	sub	r4, r4, r14

	add	r5, r5, r4, asl #1
	sub	r5, r5, r2

	add	r6, r6, r5, asl #1
	sub	r6, r6, r4

	add	r7, r7, r6, asl #1
	sub	r7, r7, r5

	add	r8, r8, r7, asl #1
	sub	r8, r8, r6

	add	r9, r9, r8, asl #1
	sub	r12, r9, r7

	add	r10, r10, r12, asl #1
	sub	r14, r10, r8

	stmia	r3!, {r2, r4 - r8, r12, r14}
	subs	r1, r1, #8
	bne	.Lorder2b
	ldmfd	r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}

	@ data[i] = residual[i] + (((data[i-1]-data[i-2])<<1)
	@	+ (data[i-1]-data[i-2])) + data[i-3];
	.macro	do_order_3, dest, res_i, b3, b2, b1, tmp
	add	\res_i, \res_i, \b3
	sub	\tmp, \b1, \b2
	add	\tmp, \tmp, \tmp, asl #1
	add	\dest, \res_i, \tmp
	.endm
.Lorder3:
	ldmdb		r3, { r11, r12, r14 }
	tst		r1, #7
	beq		.Lorder3b
.Lorder3a:
	ldr		r2, [r0], #4

	do_order_3	r2, r2, r11, r12, r14, r11
	str		r2, [r3], #4

	ldmdb		r3, { r11, r12, r14 }

	subs		r1, r1, #1
	ldmeqfd		r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}
	tst		r1, #7
	bne		.Lorder3a
.Lorder3b:
	ldmia		r0!, {r2, r4 - r10}

	do_order_3	r2, r2, r11, r12, r14, r11
	do_order_3	r4, r4, r12, r14, r2, r12
	do_order_3	r5, r5, r14, r2, r4, r14
	do_order_3	r6, r6, r2, r4, r5, r11

	do_order_3	r7, r7, r4, r5, r6, r12
	do_order_3	r11, r8, r5, r6, r7, r11
	do_order_3	r12, r9, r6, r7, r11, r12
	do_order_3	r14, r10, r7, r11, r12, r14

	stmia		r3!, {r2, r4 - r7, r11, r12, r14}
	subs		r1, r1, #8
	bne		.Lorder3b
	ldmfd		r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}

	@ data[i] = residual[i] + ((data[i-1]+data[i-3])<<2)
	@		- ((data[i-2]<<2) + (data[i-2]<<1)) - data[i-4];
	.macro	do_order_4, dest, res_i, b4, b3, b2, b1, tmp
	sub	\res_i, \res_i, \b4
	add	\tmp, \b1, \b3
	add	\res_i, \res_i, \tmp, asl #2
	sub	\res_i, \res_i, \b2, asl #2
	sub	\dest, \res_i, \b2, asl #1
	.endm
.Lorder4:
	ldmdb		r3, {r7, r11, r12, r14}
	tst		r1, #7
	beq		.Lorder4b
.Lorder4a:
	ldr		r2, [r0], #4
	do_order_4	r2, r2, r7, r11, r12, r14, r7
	str		r2, [r3], #4
	ldmdb		r3, {r7, r11, r12, r14}
	subs		r1, r1, #1
	ldmeqfd		r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}
	tst		r1, #7
	bne		.Lorder4a
.Lorder4b:
	ldr		r2, [r0], #4
	do_order_4	r2, r2, r7, r11, r12, r14, r10

	ldmia		r0!, {r4 - r10}

	do_order_4	r4, r4, r11, r12, r14, r2, r11
	do_order_4	r5, r5, r12, r14, r2, r4, r12
	do_order_4	r6, r6, r14, r2, r4, r5, r14

	do_order_4	r7, r7, r2, r4, r5, r6, r11
	do_order_4	r11, r8, r4, r5, r6, r7, r11
	do_order_4	r12, r9, r5, r6, r7, r11, r12
	do_order_4	r14, r10, r6, r7, r11, r12, r14

	stmia		r3!, {r2, r4, r5, r6, r7, r11, r12, r14}
	subs		r1, r1, #8
	bne		.Lorder4b
	ldmfd		r13!, {r4, r5, r6, r7, r8, r9, r10, r11, r12, r15}
	.size	FLAC__fixed_restore_signal_asm_arm, .-FLAC__fixed_restore_signal_asm_arm
